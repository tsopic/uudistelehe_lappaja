package klassid;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



public class NewsAnchorCounter {
	
	public List<String> uurimataAadressid = new ArrayList<>();
	public List<String> uuritudAadressid = new ArrayList<>();
	List<String> editors = new ArrayList<>();
	Map<String, Integer> uudisteankrud = new HashMap<>();
    private final int MAX_REQUESTS=20;


	int count=0;
	Boolean peatu=false;
	String url1="";
    database d;

    {
        try {
            d = new database();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public NewsAnchorCounter(String aadress){
		url1=aadress;
        try {
            System.out.println("Getting database results");
            uuritudAadressid=d.getSearchedAdresses();
            uurimataAadressid=d.getDiscoveredAdresses();
            uudisteankrud=d.getAnchors();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        uurimataAadressid.add(url1);
    }
	
	private void uuri(){
        System.out.println("uurib");
       // System.out.println(uurimataAadressid.toString());
        for (String anUurimataAadressid : uurimataAadressid) {
            uuriAadress(anUurimataAadressid);
            if (peatu) {
                break;
            }
        }
		
	}
	
	private void uuriAadress(String url){
		//System.out.println("Uurib: "+url);
		if(count<MAX_REQUESTS){
 //           if(uurimataAadressid.contains("url")){}
			
			try{
				Document doc =Jsoup.connect(url).get();
				Elements lingid = doc.select("a[abs:href]");
				Elements Ankrud = doc.select("span[itemprop=editor]");
				for(Element editor:Ankrud){
					if (editor.hasText()){
						if(uudisteankrud.containsKey(editor.text())){
							
							uudisteankrud.put(editor.text(),uudisteankrud.get(editor.text())+1);
							System.out.println(editor.text()+":"+uudisteankrud.get(editor.text()));
							
						}
						else{
							uudisteankrud.put(editor.text(), 1);
							editors.add(editor.text());
						}
						count+=1;
						System.out.println("Successful request count:"+count);
					}
					 
				}
				

				for(Element link:lingid){
					//System.out.println(link);
					String uusAadress = link.attr("abs:href");
					if(!uuritudAadressid.contains(uusAadress)&&uusAadress.contains("postimees.ee")){
                        uurimataAadressid.add(uusAadress);
					}
					//uuritudAadressid.add(link.attr("abs:href"));
					
				}
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
            try {
                sendData();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        uuritudAadressid.add(url);
	}

   /* public boolean containsURL(String uusAadress){
        if(!uusAadress.contains(url1)){

        }
        return false;
    }*/

    public void sendData() throws SQLException {
        if(!peatu) {
            peatu = true;

            int max = 0;
            String nimi = "";
         //   Map<String, Integer> a = new HashMap<String, Integer>();
            for (String editor : editors) {
                if (uudisteankrud.get(editor) > max) {
                    max = uudisteankrud.get(editor);
                    nimi = editor;
                }

            }

            System.out.println(nimi + ":" + max);
            System.out.println(uudisteankrud);
          //  System.out.println(uurimataAadressid);
            d.addAnchors(uudisteankrud, editors);

            // lisan läbivaadatud aadressid andmebaasi
            d.addSearchedAdresses(uuritudAadressid);

            // lisan läbivaatamata aadressid andmebaasi
            d.addDiscoveredAdresses(uurimataAadressid, uuritudAadressid);
        }

    }


	public static void main(String[] args) {
		NewsAnchorCounter counter = new NewsAnchorCounter("http://www.postimees.ee");
		counter.uuri();

        System.out.println("Programmi lõpp");

    }
	
}
