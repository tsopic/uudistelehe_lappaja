package klassid;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class database {

	private Connection yhendus;

    public database() throws SQLException {
        yhendus = DriverManager.getConnection("jdbc:mysql://localhost:3306/if13_news?user=root&password=secret");
    }

    public List<String> getSearchedAdresses() throws SQLException {
        PreparedStatement statement;
        statement = yhendus.prepareStatement("SELECT * from uuritudaadresid");
        ResultSet result = statement.executeQuery();
        List<String> a = new ArrayList<>();
        while(result.next()){
            a.add(result.getString(2));
        }
        return a;
    }

    public List<String> getDiscoveredAdresses() throws SQLException {
        PreparedStatement statement;
        statement = yhendus.prepareStatement("SELECT * from uurimataaadresid");
        ResultSet result = statement.executeQuery();
        List<String> a = new ArrayList<>();
        while(result.next()){
            a.add(result.getString(2));
        }
        return a;
    }

    public void addSearchedAdresses(List<String> list) throws SQLException {
        PreparedStatement statement;
        System.out.println("Lisan uuritud aadressid andmebaasi");
        statement = yhendus.prepareStatement("INSERT INTO uuritudaadresid (url) VALUES (?)");
        for(String s: list){
            statement.setString(1, s);
            statement.addBatch();
        }
        statement.executeBatch();
    }

    public void addDiscoveredAdresses(List<String> list, List<String> list2) throws SQLException {
        list = filterAadress(list, list2);
        System.out.println("Lisan uurimata aadressid andmebaasi");
        PreparedStatement statement;
        statement = yhendus.prepareStatement("INSERT INTO uurimataaadresid (url) VALUES (?)");
        for(String s: list){
            statement.setString(1, s);
            statement.addBatch();
        }
        statement.executeBatch();
    }

    public Map<String, Integer> getAnchors() throws SQLException {
        PreparedStatement statement;
        statement = yhendus.prepareStatement("Select * from ankrud");
        ResultSet result = statement.executeQuery();
        Map<String, Integer> a = new HashMap<>();
        while(result.next()){
            a.put(result.getString("nimi"), result.getInt("artikliteArv"));
            System.out.println(a);
        }
        return a;
    }

    public void addAnchors(Map<String, Integer> map, List<String> editors) throws SQLException {
        PreparedStatement statement;
        Map<String, Integer> l = getAnchors();
        Map<String, Integer> updatelist = new HashMap<>();
        List<String> updateautorid = new ArrayList<>();
        for(String a: editors){
            if(l.containsKey(a)){
                updatelist.put(a, map.get(a));
                updateautorid.add(a);
                editors.remove(a);
            }
        }

        if(updatelist.size()>0){
            statement = yhendus.prepareStatement("UPDATE ankrud SET artikliteArv=? WHERE nimi=?");
            for(int i=0; i<updateautorid.size(); i++){
                statement.setInt(1, updatelist.get(i));
                statement.setString(2, updateautorid.get(i));
                statement.addBatch();
            }
            statement.executeBatch();
        }
        statement = yhendus.prepareStatement("INSERT INTO ankrud (nimi, artikliteArv) VALUES (?, ?)");
        System.out.println("Lisan Editorid andmebaasi");

        for(String a: editors){
            statement.setString(1, a);
            statement.setInt(2, map.get(a));
            statement.addBatch();
        }
        statement.executeBatch();
    }

    private List<String> filterAadress(List<String> list, List<String> list2){
        List<String> result = new ArrayList<>(); //list = uurimata, list2 = uuritud
        for(String s: list2){
            if(!list.contains(s)){
                result.add(s);
            }
        }
        return result;
    }


}
